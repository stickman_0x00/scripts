#!/bin/bash

export CHIA_ROOT=~/.chia/mainnet

cd chia-blockchain

. ./activate

cd chia-blockchain-gui

npm run electron &