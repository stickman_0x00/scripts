#!/bin/bash

# Requirements:
# - install git pgp wget
# - have pgp key imported (https://www.getmonero.org/resources/user-guides/verification-allos-advanced.html#verify-and-import-signing-key)

# Variables
temp_folder=temp
monero_folder=monero


# Delete temp files/folders.
clean()
{
	rm -rf $temp_folder
	rm -f linux64 hashes.txt
}
# Call clean when exit
trap clean EXIT

# Remove old folder and create it again. (Delete old binaries)
rm -rf $monero_folder
mkdir $monero_folder

# Create temp folder.
mkdir $temp_folder

# Download binary.
wget https://downloads.getmonero.org/cli/linux64
wget https://www.getmonero.org/downloads/hashes.txt

gpg --verify hashes.txt
if [ $? -ne 0 ]
then
	echo "Hash.txt invalid"
	exit
fi

# Check binary.
hash=$(shasum -a 256 linux64 | cut -f 1 -d " ")
original_hash=$(cat hashes.txt | grep monero-linux-x64 | cut -f 1 -d " ")
if [ $hash != $original_hash ]
then
	echo "Binary invalid." $hash $original_hash
	exit
fi

# Extract
tar -jxvf linux64 -C $temp_folder

# Move binaries to folder
mv $temp_folder/*/* $monero_folder