#!/bin/bash

# Backup gitea server
# This should be run has gitea user
#	1 - Backup gitea
#	2 - Delete old backups

# Configuration variables
# Delete files with $days_old.
days_old=10
# Folder where backups where gonna be saved
gitea_folder=/var/lib/backups/gitea
# Config file of gitea
gitea_file=/etc/gitea/app.ini

cd $gitea_folder
# Create backup
gitea dump -c $gitea_file

# delete old backups
find $gitea_folder -maxdepth 1 -type f -mtime +$days_old -exec rm -rf {} \;
