#!/bin/bash

# Backup minecraft world:
#	1 - Zip $mine_world_folder to folder $backups_folder with name $zip_name.
#	2 - Delete old backups with more than $days_old days.

# Configuration variables
# Delete files with $days_old.
days_old=10
# Name of zip file that wil be created.
zip_name=$(date +"%d_%^b_%Y__%H_%M").zip

# Full path of folder to be backuped.
mine_world_folder="/path1/* /path2/*"
# Full path of folder where backup will be created.
backups_folder=/backups
# Full path of zip that will be created.
zip_file="$backups_folder/$zip_name"


# Compatible with "*" in argument
# Convert to array
folders=( $mine_world_folder )

# Loop folders
for folder in "${folders[@]}"
do

	if [ ! -d "$folder" ]; then
		echo "Error: $folder does not exist or is not a directory. Skipping..."
		continue
	fi

	# Enter in folder to be backup.
	cd $folder/..

	# Zip folder to backups folder.
	zip -r $zip_file $(basename $folder)
done

# Delete old backups.
find $backups_folder -maxdepth 1 -type f -mtime +$days_old -exec rm -rf {} \;