#!/bin/bash

# MD to HTML using pandoc

browser=firefox

# create temp file
tempFile=$(mktemp)
# delete file when exit
trap 'rm -f $tempFile' EXIT

# convert a md file to html
pandoc $1 > $tempFile
# open it
$browser $tempFile > /dev/null 2>&1 &

sleep 3
