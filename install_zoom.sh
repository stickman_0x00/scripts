#!/bin/bash

# Install zoom in Debian

ZOOM_FILE=zoom.deb

# Download
curl -L https://zoom.us/linux/latest/zoom_amd64.deb -o $ZOOM_FILE

# Install
sudo apt install ./$ZOOM_FILE

# Delete file
rm $ZOOM_FILE

# Firejail part
# Backup executable
sudo mv /usr/bin/zoom /usr/bin/zoom.bak

# Create new executable (firejail)
sudo touch /usr/bin/zoom
sudo tee /usr/bin/zoom > /dev/null <<EOF
#!/bin/bash
firejail --profile=/etc/firejail/zoom.profile --private=/opt/zoom/home -c -- /opt/zoom/ZoomLauncher "\$@"
EOF

# Give permissions
sudo chmod +x /usr/bin/zoom
