#!/bin/bash

# Set fan speed %
speed=43

nvidia-settings -a "[gpu:0]/GPUFanControlState=1"
nvidia-settings -a "[fan:0]/GPUTargetFanSpeed=$speed"
