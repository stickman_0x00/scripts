#!/bin/bash

# Simple script to check if arch linux needs manual update.
# Exaple:
#	alias update='$0; [ $? -eq 0 ] && sudo pacman -Syu'

# File that will save the news.
file="$HOME/.archUpdates.xml"
# Link to get the news.
updates=$(curl --silent https://www.archlinux.org/feeds/news/)
exit_value=0

# Check for internet connection.
ip=$(ping -c 1 archlinux.org &>/dev/null; echo $?)
if test -z "$ip"
then
	echo "No internet connection"
	exit 2
fi

# Check if file exist.
if [ -f $file ]
then
	# File exist.
	oldUpdates=$(cat $file)
	if [ "$(echo $oldUpdates)" == "$(echo $updates)" ]
	then
		# No new update.
		echo "Same updates, nothing to check."
	else
		echo "New updates! Visit https://archlinux.org."
		exit_value=1
	fi
else
	# File doesn't exist.
	echo "File didn't exist, visit https://archlinux.org."
	exit_value=1
fi

# Save new updates.
echo $updates > $file

# Return exit value.
exit $exit_value
