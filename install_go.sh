#!/bin/bash

# Install golang in local user folder (~/.local/go)

if [ ${#} -ne 1 ]
then
	echo "This script has 1 parameter"
	echo "$0 <go_version>"
	exit
fi

VERSION=$1
GO_FILE=go.tar.gz
LOCAL=$HOME/.local

cd $LOCAL
rm -rf go

curl -L https://golang.org/dl/go$VERSION.linux-amd64.tar.gz -o $GO_FILE
tar -C $LOCAL -xzf $GO_FILE

rm $GO_FILE