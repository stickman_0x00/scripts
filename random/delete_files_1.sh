#!/bin/bash

# loop through file with folders path and remove the
# files that have more than $X days
#
# HOW TO
# create a file with the same name of the script
# ex: delete_files_1.sh delete_files_1.txt

if [ ${#} -ne 1 ]
then
	echo "This script has 1 argument"
	echo "$0 <days>"
	exit
fi

# remove extension and add .txt
folder_file="$(pwd)/$(echo "$0" | cut -f 1 -d '.').txt"

# loop through file with folders path
for folder in $(cat $folder_file)
do
	if [[ ! -d $folder ]]; then
		# if folder doesn't exist report to the user
		echo Not a directory: $folder
		continue
	fi

	# find and delete the files from folder
	find $folder -mtime +$1 -exec rm -rf {} \;
	echo Files deleted from folder: $folder
done