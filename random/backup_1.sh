#!/bin/bash

# this is a script to be used on crontab
# this will copy file to a system
# if everything runs good, than the file is archive
# if not file is moved to a error folder
# logs are written

# STATIC VARIABLE
ERRORDIR=/error
PERMISSION=777
LOGERRORFILE=/logs/log_actout_ERROR.log
LOGOKFILE=/logs/log_actout_OK.log

# Check if all parameters are given
if [ ${#} -ne 4 ]
then
	echo "This script has 4 parameters"
	echo "$0 <Sourcedir> <IP> <PASSWORD> <Archive Directory>"
	exit
fi

SOURCEDIR=$1;
IP=$2;
PASSWORD=$3;
ARCHIVE=$4;

date=$(date "+%Y%m%d-%H%M%S")

# enter in source foder
cd $SOURCEDIR

# get files
files=$(find . -maxdepth 1 -mindepth 1 -type f -printf '%f\n')

# loop through files
for FILE in $files
do
	# output file being processed
	echo $FILE

	# change permissions of file
	chmod $PERMISSION $FILE

	# copy file
	/opt/bin/ncopy -c -b -k $FILE $IP!$FILE $PASSWORD

	# get the result of the copy
	result=$?

	if [ $result -eq 0 ]
	then
		# if copy was successful

		# mv file to archive and change name ex: name.txt -> $date_name.txt
		mv $FILE $ARCHIVE/$date\_$FILE

		# output to log success file
		echo "`date`: result = $result for $SOURCEDIR/$FILE" >> $LOGOKFILE
	else
		# if copy was not successful

		# mv file to error folder
		mv $FILE $ERRORDIR

		# output to log error file
		echo "`date`: result = $result for $SOURCEDIR/$FILE" >> $LOGERRORFILE
	fi
done