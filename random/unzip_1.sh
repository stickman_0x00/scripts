#!/bin/bash

# This script enters in a directory with zip files
# loop through the zip files
# unzip the files to prod folder
# if successfull send the zip to backup folder

if [ ${#} -ne 1 ]
then
	echo "This script has 1 parameters"
	echo "$0 <Sourcedir>"
	exit
fi

# STATIC VARIABLE
backup_folder=bkp
prod_folder=prod

SOURCE_DIR=$1

# Enter in folder.
cd $SOURCE_DIR

# Search for zip files only in the current directory
files=$(find . -maxdepth 1 -mindepth 1 -type f -name '*.zip' -printf '%f\n')

# Loop through files.
for FILE in $files
do
	# output zip file being processed
	echo $FILE

	unzip -q $FILE -d ./$prod_folder

	# get the result of the unzip
	result=$?
	if [ $result -eq 0 ]
	then
		# If the unzip was successful move zip to backup directory
		mv $FILE $backup_folder/
	fi
done
